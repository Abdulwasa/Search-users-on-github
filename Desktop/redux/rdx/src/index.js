import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import  reducers from './index-reducer'
import {createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';

const aplay = compose(
	applyMiddleware(thunk))

const store = createStore(reducers, aplay)
ReactDOM.render(
			<Provider store={store}>
				<App />
			</Provider>	,
				document.getElementById('root'));
registerServiceWorker();
